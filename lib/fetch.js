"use strict";

const nodeFetch = require("node-fetch");
const Promise = require("bluebird");
nodeFetch.Promise = Promise;

const fetch = require("make-fetch-happen").defaults({
  cacheManager: process.cwd() + "/cache",
  retry: {
    retries: 3,
    randomize: true
  }
});

module.exports = fetch;
