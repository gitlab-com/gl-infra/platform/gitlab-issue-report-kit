"use strict";

const httpClient = require("./http-client");

async function cleanOldReports (currentIid, report) {
  const [openReports] = await httpClient(
    `https://gitlab.com/api/v4/projects/${encodeURIComponent(report.project)}/issues?state=opened&scope=all&labels=${encodeURIComponent(report.label)}&confidential=${report.confidential}`
  );

  const overdueReports = openReports.filter((issue) => issue.iid !== currentIid && issue.title.startsWith(report.titlePrefix));

  for (const odr of overdueReports) {
    await httpClient(`https://gitlab.com/api/v4/projects/${encodeURIComponent(report.project)}/issues/${odr.iid}?state_event=close`, "PUT");
  }
}

module.exports = cleanOldReports;
