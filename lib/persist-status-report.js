/* eslint-disable no-console */
"use strict";

const collectResults = require("./collect-results");
const httpClient = require("./http-client");
const subDays = require("date-fns/sub_days");
const startOfDay = require("date-fns/start_of_day");
const format = require("date-fns/format");
const addDays = require("date-fns/add_days");

async function findStatusReport (report) {
  const since = report.since;
  const isoSince = format(startOfDay(subDays(since, 1)), "YYYY-MM-DD");
  const encodedProjectUri = encodeURIComponent(report.project);
  const encodedLabel = encodeURIComponent(report.label);

  const statusReports = await collectResults(
    `https://gitlab.com/api/v4/projects/${encodedProjectUri}/issues?labels=${encodedLabel}&order_by=created_at&sort=desc&created_after=${isoSince}&confidential=${report.confidential}`
  );
  return statusReports[0];
}

function getTitle (report) {
  const from = format(startOfDay(report.since), "YYYY-MM-DD");
  const to = format(addDays(startOfDay(report.since), 7), "YYYY-MM-DD");

  return `${report.titlePrefix}${from} - ${to}`;
}

async function createNewReport (report) {
  const title = getTitle(report);
  const encodedProjectUri = encodeURIComponent(report.project);

  const [body] = await httpClient(`https://gitlab.com/api/v4/projects/${encodedProjectUri}/issues`, "POST", {
    labels: report.label,
    title,
    confidential: report.confidential,
    description: report.body
  });

  return body;
}

async function updateExistingStatusReport (existing, report) {
  const title = getTitle(report);
  const encodedProjectUri = encodeURIComponent(report.project);

  if (existing.description === report.body) {
    console.error("# Report description matches");
    return;
  }

  const [body] = await httpClient(`https://gitlab.com/api/v4/projects/${encodedProjectUri}/issues/${existing.iid}`, "PUT", {
    title,
    description: report.body
  });

  return body;
}

// Returns the IID of the status report
async function persistStatusReport (report) {
  const existing = await findStatusReport(report);
  if (existing) {
    await updateExistingStatusReport(existing, report);
    return existing;
  }

  const newReportIssue = await createNewReport(report);
  return newReportIssue;
}

module.exports = persistStatusReport;
