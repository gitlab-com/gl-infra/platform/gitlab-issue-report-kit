"use strict";

const parseLinks = require("parse-links");
const httpClient = require("./http-client");

const DEFAULT_PERPAGE = 100;

function getTotalPages (headers) {
  const link = headers.get("link");
  if (!link) return;
  const links = parseLinks(link);

  if (!links.last) return;
  const parsed = new URL(links.last);
  const page = parsed.searchParams.get("page");
  return parseInt(page, 10);
}

async function * streamAllPages (address, stopper) {
  const urlObject = new URL(address);

  let perPage;

  if (urlObject.searchParams.has("per_page")) {
    perPage = urlObject.searchParams.get("per_page");
  } else {
    perPage = DEFAULT_PERPAGE;
    urlObject.searchParams.set("per_page", perPage);
  }

  const initialPage = urlObject.searchParams.get("page") || 1;
  let count = 0;

  while (true) {
    const page = initialPage + count++;
    urlObject.searchParams.set("page", page);

    const formattedUrl = urlObject.href;

    const [results, response] = await httpClient(formattedUrl);
    const lastPage = getTotalPages(response.headers) || page;

    // No results? End the stream
    if (!results || !results.length) {
      return;
    }

    for (let i = 0; i < results.length; i++) {
      const result = results[i];

      if (stopper && stopper(result)) {
        return;
      }

      yield result;
    }

    // If we get back less than one page of results,
    // or we're on the last page, end the stream
    if (results.length < perPage || page === lastPage) {
      return;
    }
  }
}

module.exports = streamAllPages;
