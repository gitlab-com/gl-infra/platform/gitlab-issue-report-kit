/* eslint-disable no-console */
"use strict";

const Promise = require("bluebird");
const cacache = require("cacache");

const MAX_AGE_DAYS = 3;

async function purgeCache () {
  const cachePath = process.cwd() + "/cache";
  const entries = await cacache.ls(cachePath);

  const deletions = Object.keys(entries).filter((key) => {
    const entry = entries[key];
    const age = Date.now() - entry.time;
    return age >= MAX_AGE_DAYS * 86400 * 1000;
  });

  if (deletions.length) {
    await Promise.map(deletions, (key) => cacache.rm.entry(cachePath, key), { concurrency: 5 });
  }

  await cacache.verify(cachePath);
}

module.exports = purgeCache;
