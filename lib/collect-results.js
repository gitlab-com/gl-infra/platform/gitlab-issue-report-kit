"use strict";

const urlStreamer = require("./url-streamer");

async function collectResults (url, stopper) {
  const collect = [];

  for await (const result of urlStreamer(url, stopper)) {
    collect.push(result);
  }

  return collect;
}

module.exports = collectResults;
