/* eslint-disable no-console */
"use strict";

const fetch = require("./fetch");

const gitlabToken = process.env.GITLAB_TOKEN;
const gitlabOAuthToken = process.env.GITLAB_OAUTH_TOKEN;

function injectHeadersForAddress (address, headers) {
  if (!address.startsWith("https://gitlab.com/")) return headers;

  if (gitlabToken) {
    headers["Private-Token"] = gitlabToken;
  }

  if (gitlabOAuthToken) {
    headers.Authorization = "Bearer " + gitlabOAuthToken;
  }

  return headers;
}

async function get (address, method, body) {
  console.error(`# ${method || "GET"} ${address}`);

  const options = {
    method: method || "GET",
    compress: true,
    headers: injectHeadersForAddress(address, {
      "Content-Type": "application/json",
      Accept: "application/json"
    })
  };

  if (body) {
    options.body = JSON.stringify(body);
  }

  const res = await fetch(address, options);
  console.error(`# ${res.status} ${res.statusText}`);

  if (!res.ok && res.status !== 304) {
    let errorMessage = res.statusText;
    try {
      const errorResponse = await res.json();
      if (errorResponse && errorResponse.message && errorResponse.message.error) {
        errorMessage = errorResponse.message.error;
      }
    } catch (e) {
      // Can't decode json response. Ignore
    }

    throw new Error(errorMessage);
  }

  const responseBody = await res.json();
  return [responseBody, res];
}

module.exports = get;
