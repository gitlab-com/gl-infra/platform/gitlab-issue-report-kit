"use strict";

class Report {
  constructor (since, project, label, confidential, titlePrefix, body) {
    this.since = since;
    this.project = project;
    this.label = label;
    this.confidential = confidential;
    this.titlePrefix = titlePrefix;
    this.body = body;
  }
}

module.exports = Report;
