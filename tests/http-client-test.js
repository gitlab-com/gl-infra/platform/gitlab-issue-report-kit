/* eslint-disable no-undef */
const assert = require("assert");

const httpClient = require("../lib/http-client");

describe("http-client", function () {
  describe("#get()", function () {
    it("should return a valid URL", async function () {
      const commits = await httpClient("http://gitlab.com/api/v4/projects/gitlab-com%2fgl-infra%2fplatform%2fgitlab-issue-report-kit/repository/commits");
      assert.equal(true, commits.length > 0);
    });
  });
});
